<?php

/**
 * @file
 * Definition of the 'Accordion' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Accordion'),
  'description' => t('Show panel panes in a region as Accordion'),
  'render region' => 'panels_accordion_style_render_region',
  'render pane' => 'panels_accordion_style_render_pane',
  'settings form' => 'panels_accordion_style_settings_form',
);

function theme_panels_accordion_style_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  if (empty($content->content)) {
    return;
  }
  $region_id = $pane->panel;
  $header = '';
  if (!empty($content->title)) {
    $header = "<h3>{$content->title}</h3>";
    unset($content->title);
  }
  $tab_id = 'tabs-' . $region_id;
  $output['pane'] = array(
    '#type' => 'markup',
    '#markup' => theme('panels_pane', array('content' => $content, 'pane' => $pane, 'display' => $display)),
    '#prefix' => $header . '<div id="' . $tab_id . '-' . $pane->position . '">',
    '#suffix' => '</div>',
  );
  return render($output);
}

function theme_panels_accordion_style_render_region($vars) {
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $style_settings = $vars['settings'];
  $tab_id = 'tabs-' . $region_id;

  $element['accordion_content'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="' . $tab_id . '">',
    '#suffix' => '</div>',
    '#attached' => array(
      'library' => array(array('system', 'ui.accordion')),
      'js' => array(
        drupal_get_path('module', 'pp_accordion_style') . '/js/pp_accordion_style.js' => array('type' => 'file'),
      ),
      'css' => array(
        drupal_get_path('module', 'pp_accordion_style') . '/css/pp_accordion_style.css',
      ),
    ),
  );

  $settings = array();
  $settings['panelsAccordion']['accordionID'][] = $tab_id;
  $settings['panelsAccordion']['collapsible'] = $style_settings['collapsible'];
  $settings['panelsAccordion']['heightstyle'] = $style_settings['heightstyle'];

  $settings['type'] = 'accordion';


  $element['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => $settings,
  );

  foreach ($panes as $pane_id => $item) {
    $element['accordion_content'][$pane_id] = array(
      '#markup' => $item,
    );
  }

  return drupal_render($element);
}

function panels_accordion_style_settings_form($style_settings) {
  $form = array();

  $form['collapsible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsible'),
    '#default_value' => $style_settings['collapsible'],
    '#description' => t('Select if the acordion will be collapsible.')
  );

  $form['heightstyle'] = array(
    '#type' => 'select',
    '#title' => t('Height Style'),
    '#default_value' => $style_settings['heightstyle'],
    '#options' => array(
      'fill' => 'Fill',
      'content' => 'No Auto Height',
    ),
    '#description' => t('Select an applicable heightstyle'),
  );
  return $form;
}